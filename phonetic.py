# phonetic.py
# Copyright Paul Whipp 2010
#
# Convert input string into a 'phonetic' string using
# NATO phonetic alphabet (plus upper/lower for case).
# Handy for reading passwords and the like over the phone.
# None converted characters are left alone

# Phonetic dictionary
nato_phonetic = dict(
    A='ALPHA',
    B='BRAVO',
    C='CHARLIE',
    D='DELTA',
    E='ECHO',
    F='FOXTROT',
    G='GOLF',
    H='HOTEL',
    I='INDIA',
    J='JULIET',
    K='KILO',
    L='LIMA',
    M='MIKE',
    N='NOVEMBER',
    O='OSCAR',
    P='PAPA',
    Q='QUEBEC',
    R='ROMEO',
    S='SIERRA',
    T='TANGO',
    U='UNIFORM',
    V='VICTOR',
    W='WHISKEY',
    X='X-RAY',
    Y='YANKEE',
    Z='ZULU')

nato_phonetic['.']='POINT'

def phonetic(input):
    """Return a string of the form 'Alpha - Zero' for an input string of the form 'A0'"""

    output = ''
    for c in input:

        # Pick the phoneme
        k = c.upper()
        if k in nato_phonetic:
            phoneme = nato_phonetic[k]
        else:
            phoneme = c
    
        # Lower phoneme if c is lower (in case verbal distinction e.g. lower/upper needed)
        if c.islower(): phoneme=phoneme.lower()
        output = output + phoneme + ' - '

    return output[:-3]

if __name__ == "__main__":
    import sys
    print(phonetic(sys.argv[1]))
